<?php

require_once('../lib/Sabre/autoload.php');

$rootdir = new Sabre_DAV_FS_Directory('../source');
$server  = new Sabre_DAV_Server($rootdir);

$lock_backend = new Sabre_DAV_Locks_Backend_File('../locks/');
$lock_plugin  = new Sabre_DAV_Locks_Plugin($lock_backend);

$server->addPlugin($lock_plugin);

$browser_plugin = new Sabre_DAV_Browser_Plugin();
$server->addPlugin($browser_plugin);

$user = 'nyarla';
$pass = '12345';

$auth = new Sabre_HTTP_DigestAuth();
$auth->init();

if ( $auth->getUsername() == $user && $auth->validatePassword($pass) ) {
   $server->exec();
}
else {
     $auth->requireLogin();
     echo "Authentication required!\n";
     die();
}

?>